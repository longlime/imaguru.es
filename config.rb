require 'autoprefixer-rails'
require 'compass/import-once/activate'
require 'uglifier'
require 'csso'
require 'slim'

###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (https://middlemanapp.com/advanced/dynamic_pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
# configure :development do
#   activate :livereload
# end

# Methods defined in the helpers block are available in templates
helpers do
  def inline_stylesheet(name)
    content_tag :style do
      AutoprefixerRails.install(sprockets)
      code = sprockets[ "#{name}.css" ].to_s

      if config[:environment] == :build
        code = Csso.optimize(code)

        # This fucking hack for correct `relative_assets` extension!
        code = code.gsub('../../../fonts/', '../assets/fonts/')
        code = code.gsub('../images/', '../assets/images/')
      else
        code
      end
    end
  end

  def inline_javascript(name)
    content_tag :script do
      code = sprockets[ "#{name}.js" ].to_s

      if config[:environment] == :build
        Uglifier.compile(code)
      else
        code
      end
    end
  end

  def retina_img(name, alt='')
    img_sm = asset_url("#{name}.png")
    img_lg = asset_url("#{name}@2x.png")

    content_tag :img, "", {src: img_sm, srcset: "#{img_lg} 2x", alt: alt}
  end
end

set :build_dir,   'build'

set :source,      'app'
set :css_dir,     'assets/stylesheets'
set :js_dir,      'assets/javascripts'
set :images_dir,  'assets/images'
set :layout,      'application'
set :layouts_dir, 'views/layouts'

# set :slim, { pretty: true }

sprockets.append_path File.join root, config[:source], 'frontend'
sprockets.append_path File.join root, 'bower_components'

activate :autoprefixer

activate :i18n, :mount_at_root => :en

configure :development do
  set :host, 'localhost'
  set :port, 3000
end

configure :build do
  activate :minify_css
  activate :minify_javascript

  activate :htmlbeautifier

  # Enable cache buster
  activate :asset_hash

  # Use relative URLs
  activate :relative_assets

  # Or use a different image path
  # set :http_prefix, ".."
end
