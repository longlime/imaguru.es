/*

smartresize(function() {
  // Handle the resize event here
});

// Executing after loading
smartresize(function() {
  // Handle the resize event here
})();

*/

window.smartresize = function(callback, timer) {
    window.addEventListener('resize', function() {
        clearTimeout(timer);
        timer = setTimeout(callback, timer);
    }, false);

    return callback;
};
