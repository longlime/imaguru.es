(function() {

    var scrolling, onload, $logotypes, $sections;
    var pull = 110;

    scrolling = function() {
        var isPassed, isActive, $section, $logotype, sectionOffsetTop;
        var i = 0;
        var scrollTop = window.scrollY;

        for (; i < $sections.length; i++) {
            $section = $sections[i];
            $logotype = $logotypes[i];
            sectionOffsetTop = $section.offsetTop;

            // Because last section hasnt logotype
            if ($logotype) {
                isPassed = scrollTop > sectionOffsetTop + $section.offsetHeight - $logotype.__pull;

                // Prelast section cannot be `passed`
                if (i === $logotypes.length - 1) {
                    isPassed = false;
                }

                isActive = scrollTop > sectionOffsetTop && !isPassed;

                $section.classList[isPassed ? 'add' : 'remove']('-passed');
                $section.classList[isActive ? 'add' : 'remove']('-active');
            }
        }
    };

    onload = function() {
        var pull;
        var i = 0;

        $logotypes = document.querySelectorAll('.section__logo');
        $sections = document.querySelectorAll('.section');

        for (; i < $logotypes.length; i++) {
            pull = parseInt(window.getComputedStyle($logotypes[i], null).getPropertyValue('top')) || parseInt(window.getComputedStyle($logotypes[i], null).getPropertyValue('bottom'));
            pull = pull * 2 + $logotypes[i].offsetHeight;

            $logotypes[i].__pull = pull;
        }

        window.addEventListener('scroll', scrolling, false);
        scrolling();
    };

    // Because javascript inserted after html code
    onload();

    smartresize(function() {
       scrolling();
    });

})();
