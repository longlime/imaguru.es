(function() {

    var scrolling, onload, $section;

    scrolling = function() {
        var scrollTop = window.scrollY;
        var isPassed = scrollY > $section.offsetTop;

        $section.classList[isPassed ? 'add' : 'remove']('-fixed-prelast');
    };

    onload = function() {
        $section = document.querySelector('#prelast');

        window.addEventListener('scroll', scrolling, false);
    };

    // Because javascript inserted after html code
    onload();

})();
